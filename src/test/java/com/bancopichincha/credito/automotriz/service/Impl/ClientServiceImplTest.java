package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.ClientDTO;
import com.bancopichincha.credito.automotriz.domain.entities.Client;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ClientServiceImplTest {
    @Mock
    ClientRepository clientRepositoryMock;

    @InjectMocks
    ClientServiceImpl clientService;

    ClientDTO clientDTO;
    Client client;

    UUID id;

    @BeforeEach
    void setUp() {
        id = UUID.fromString("fc942408-4cca-4d1c-8ff0-b556f18ec58f");
        clientDTO = new ClientDTO();
        clientDTO.setId(id);
        clientDTO.setDni("1722812037");
        clientDTO.setFirstName("Alexa");
        clientDTO.setAddress("Conocoto");
        clientDTO.setAge(28);
        clientDTO.setCivilStatus("Soltera");
        client = new Client();
        client.setId(id);
        client.setDni("1722812037");
        client.setFirstName("Alexa");
        client.setAddress("Conocoto");
        client.setAge(28);
        client.setCivilStatus("Soltera");
    }

    @Test
    void getAll() {
        List<Client> clients = List.of(client);
        when(clientRepositoryMock.findAll()).thenReturn(clients);
        List<ClientDTO> result = clientService.getAll();
        assertNotNull(result);
        assertEquals(clients.size(), result.size());
        assertEquals(clients.get(0).getId(), result.get(0).getId());
        assertEquals(client.getId(), result.get(0).getId());
    }

}
