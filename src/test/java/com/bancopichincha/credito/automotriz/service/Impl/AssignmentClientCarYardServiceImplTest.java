package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.AssignmentClientCarYardDTO;
import com.bancopichincha.credito.automotriz.domain.dto.CarYardDTO;
import com.bancopichincha.credito.automotriz.domain.dto.ClientDTO;
import com.bancopichincha.credito.automotriz.domain.entities.AssignmentClientCarYard;
import com.bancopichincha.credito.automotriz.domain.entities.CarYard;
import com.bancopichincha.credito.automotriz.domain.entities.Client;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.AssignmentClientCarYardRepository;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AssignmentClientCarYardServiceImplTest {
    @Mock
    AssignmentClientCarYardRepository assignmentClientCarYardRepository;

    @InjectMocks
    AssignmentClientCarYardServiceImpl assignmentClientCarYardService;

    AssignmentClientCarYardDTO assignmentClientCarYardDTO;
    AssignmentClientCarYard assignmentClientCarYard;

    UUID id1;
    UUID id2;
    UUID id3;

    ClientDTO clientDTO;
    Client client;
    CarYardDTO carYardDTO;
    CarYard carYard;

    @BeforeEach
    void setUp(){
        id1 = UUID.fromString("fc942408-4cca-4d1c-8ff0-b556f18ec58f");
        id2 = UUID.fromString("f8eb462f-4927-4092-bb6b-36fdef2f0393");
        id3 = UUID.fromString("50a845b1-bbb8-4be3-baed-e55c09206f10");
        assignmentClientCarYardDTO = new AssignmentClientCarYardDTO();
        assignmentClientCarYard = new AssignmentClientCarYard();
        clientDTO = new ClientDTO();
        clientDTO.setId(id1);
        clientDTO.setDni("1722812037");
        clientDTO.setFirstName("Alexa");
        clientDTO.setAddress("Conocoto");
        clientDTO.setAge(28);
        clientDTO.setCivilStatus("Soltera");
        client = new Client();
        client.setId(id1);
        client.setDni("1722812037");
        client.setFirstName("Alexa");
        client.setAddress("Conocoto");
        client.setAge(28);
        client.setCivilStatus("Soltera");
        carYardDTO = new CarYardDTO();
        carYardDTO.setId(id2);
        carYardDTO.setName("Casabaca");
        carYardDTO.setAddress("San Pedro");
        carYardDTO.setPhone("2354673");
        carYardDTO.setNumberPoint(203);
        carYard = new CarYard();
        carYard.setId(id2);
        carYard.setName("Casabaca");
        carYard.setAddress("San Pedro");
        carYard.setPhone("2354673");
        carYard.setNumberPoint(203);
        assignmentClientCarYardDTO.setId(id3);
        assignmentClientCarYardDTO.setCarYardDTO(carYardDTO);
        assignmentClientCarYardDTO.setClientDTO(clientDTO);
        assignmentClientCarYardDTO.setAssignmentDate(new Date());
        assignmentClientCarYard.setId(id3);
        assignmentClientCarYard.setCarYard(carYard);
        assignmentClientCarYard.setClient(client);
        assignmentClientCarYard.setAssignmentDate(new Date());

    }

    @Test
    void saveOk() {
        when(assignmentClientCarYardRepository.findByClientAndCarYard(any(), any())).thenReturn(Optional.empty());
        when(assignmentClientCarYardRepository.save(any())).thenReturn(assignmentClientCarYard);
        AssignmentClientCarYardDTO result = assignmentClientCarYardService.save(assignmentClientCarYardDTO);
        assertNotNull(result);
        assertEquals(assignmentClientCarYard.getId(),result.getId());
        verify(assignmentClientCarYardRepository,times(1)).findByClientAndCarYard(any(), any()); //# veces que se ejecuto un metodo
    }

    @Test
    void saveFail() {
        when(assignmentClientCarYardRepository.findByClientAndCarYard(any(), any())).thenReturn(Optional.of(assignmentClientCarYard));
        Throwable exception = assertThrows(ResponseException.class, () -> assignmentClientCarYardService.save(assignmentClientCarYardDTO));
        assertEquals("Cliente ya esta asignado a ese patio", exception.getMessage());
    }
}
