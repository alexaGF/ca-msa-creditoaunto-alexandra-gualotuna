package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.CarYardDTO;

import com.bancopichincha.credito.automotriz.domain.entities.CarYard;
import com.bancopichincha.credito.automotriz.repository.CarYardRepository;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
public class CarYardControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CarYardRepository carYardRepository;
    @Autowired
    private ObjectMapper objectMapper;

    CarYardDTO carYardDTO;
    CarYard carYard;
    CarYard carYardInitial;
    UUID id, id2, id3;
    String idS, idS2;

    @BeforeEach
    void setUp() {
        idS = "13290600-8e94-40f3-8f9b-83db3a587ba8";
        idS2 = "f8eb462f-4927-4092-bb6b-36fdef2f0393";
        id = UUID.fromString(idS);
        id2 = UUID.fromString(idS2);
        carYardDTO = new CarYardDTO();
        carYardDTO.setId(id);
        carYardDTO.setName("Casabaca");
        carYardDTO.setAddress("San Pedro");
        carYardDTO.setPhone("2354673");
        carYardDTO.setNumberPoint(203);
        carYard = new CarYard();
        carYard.setId(id);
        carYard.setName("Casabaca");
        carYard.setAddress("San Pedro");
        carYard.setPhone("2354673");
        carYard.setNumberPoint(203);
        carYardInitial = carYardRepository.save(carYard);
    }


    @Test
    void saveCarYard() throws Exception {

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders
                .post("/carYards")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(carYardDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.name").exists())
                .andExpect(jsonPath("$.address").exists())
                .andExpect(jsonPath("$.phone").exists())
                .andExpect(jsonPath("$.numberPoint").exists())
                .andReturn();

        /*CarYardDTO result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), CarYardDTO.class);
        assertNotNull(result);*/

    }

    @Test
    void getAllCarYard() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/carYards"))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name", is("Casabaca")))
                .andExpect(jsonPath("$[0].address", is("San Pedro")))
                .andExpect(jsonPath("$[0].phone", is("2354673")))
                .andExpect(jsonPath("$[0].numberPoint", is(203)));
    }

    @Test
    void getCarYard() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/carYards/" + carYardInitial.getId().toString() ))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("Casabaca")))
                .andExpect(jsonPath("$.address", is("San Pedro")))
                .andExpect(jsonPath("$.phone", is("2354673")))
                .andExpect(jsonPath("$.numberPoint", is(203)));
    }

    @Test
    void updateCarYard() throws Exception {
        carYardInitial.setName("Casabaca2");
        mockMvc.perform(MockMvcRequestBuilders
                        .put("/carYards")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(carYardInitial)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteCarYard() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/carYards/" + carYardInitial.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(content().string("Patio eliminado correctamente"));
    }
}

