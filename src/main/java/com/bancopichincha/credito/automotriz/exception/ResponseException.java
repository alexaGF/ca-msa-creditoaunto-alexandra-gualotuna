package com.bancopichincha.credito.automotriz.exception;

import org.springframework.http.HttpStatus;

public class ResponseException extends RuntimeException {
    private HttpStatus httpStatus;
    public ResponseException(String message) {super(message);}

    public ResponseException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResponseException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
