package com.bancopichincha.credito.automotriz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseHandler {
    public static ResponseEntity validateException(String message, HttpStatus status){
            Map<String, String> map = new HashMap<>();
            map.put("Error", message);
            ResponseEntity<Map> response = new ResponseEntity<Map>(map, status);
            return response;
    }

}
