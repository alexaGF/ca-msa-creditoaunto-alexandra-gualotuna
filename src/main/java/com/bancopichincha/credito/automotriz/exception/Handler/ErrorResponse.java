package com.bancopichincha.credito.automotriz.exception.Handler;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class ErrorResponse implements Serializable {
    private String message;
    private LocalDateTime date;
    private HttpStatus status;

    public ErrorResponse(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
        date = LocalDateTime.now();
    }

}