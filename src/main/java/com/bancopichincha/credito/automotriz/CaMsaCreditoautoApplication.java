package com.bancopichincha.credito.automotriz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class CaMsaCreditoautoApplication {
	public static void main(String[] args) {
		SpringApplication.run(CaMsaCreditoautoApplication.class, args);
	}

}
