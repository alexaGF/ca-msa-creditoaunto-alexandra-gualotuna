package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.BrandDTO;
import com.bancopichincha.credito.automotriz.domain.entities.Brand;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.service.BrandService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BrandServiceImpl implements BrandService {
    private final BrandRepository brandRepository;

    private ModelMapper mapper = new ModelMapper();
    @PostConstruct
    public void loader() {
        this.loadCSV();
    }

    public void loadCSV(){
        FileReader fileCSV = null;
        CSVReader csvReader = null;
        try {
            String[] data = null;
            List<Brand> brands = new ArrayList<>();
            Resource resource = new ClassPathResource("/initialData/brands.csv");
            fileCSV = new FileReader(resource.getURL().getPath());
            CSVParser csvParser = new CSVParserBuilder().withSeparator(',').build();
            csvReader = new CSVReaderBuilder(fileCSV).withCSVParser(csvParser).build();

            while ((data = csvReader.readNext()) != null) {
                brands.add(cvsToBrand(data));
            }
            brandRepository.saveAll(brands);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public BrandDTO save(BrandDTO brandDTO) {
        Brand brand = brandRepository.findById(brandDTO.getId()).orElse(null);
        if(brand == null){
            brandRepository.save(convertEntity(brandDTO));
            return brandDTO;
        } else {
            throw new ResponseException("Marca ya registrada");
        }
    }

    @Override
    public BrandDTO get(String id) {
        Optional<Brand> brand = brandRepository.findById(UUID.fromString(id));
        if(!brand.isPresent()){
            throw new ResponseException("Marca no encontrada");
        }
        return convertDTO(brand.get());
    }

    @Override
    public List<BrandDTO> getAll() {
        List<BrandDTO> brandDTOS = new ArrayList<>();
        List<Brand> brands = brandRepository.findAll();
        brands.forEach(brand -> {
            brandDTOS.add(convertDTO(brand));
        });
        return brandDTOS;
    }

    @Override
    public BrandDTO update(BrandDTO brandDTO) {
        Optional<Brand> brandQuery = brandRepository.findById(brandDTO.getId());
        if(!brandQuery.isPresent()){
            throw new ResponseException("Marca no encontrada");
        }
        Brand brand = convertEntity(brandDTO);
        brandRepository.save(brand);
        return convertDTO(brand);
    }

    @Override
    public String delete(String id) {
        Optional<Brand> brand = brandRepository.findById(UUID.fromString(id));
        if(!brand.isPresent()){
            throw new RuntimeException("Marca no encontrada");
        }
        brandRepository.deleteById(UUID.fromString(id));
        return "Marca eliminada correctamente";
    }


    private BrandDTO convertDTO(Brand brand){
        return mapper.map(brand, BrandDTO.class);
    }
    private Brand convertEntity(BrandDTO BrandDTO){
        return mapper.map(BrandDTO, Brand.class);
    }

    private Brand cvsToBrand(String[] data) throws ParseException {
        Brand brand = new Brand();
        brand.setName(data[0]);
        return brand;
    }
}
