package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.domain.dto.RequestCreditDTO;
import com.bancopichincha.credito.automotriz.domain.dto.VehicleDTO;
import com.bancopichincha.credito.automotriz.domain.entities.RequestCredit;
import com.bancopichincha.credito.automotriz.domain.entities.Vehicle;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.repository.RequestCreditRepository;
import com.bancopichincha.credito.automotriz.service.RequestCreditService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class RequestCreditServiceImpl implements RequestCreditService {

    private final RequestCreditRepository requestCreditRepository;

    private final ExecutiveRepository executiveRepository;

    private ModelMapper mapper = new ModelMapper();

    @Transactional
    @Override
    public RequestCreditDTO save(RequestCreditDTO requestCreditDTO) {
        RequestCredit requestCreditV = requestCreditRepository.findByVehicleJPQL(requestCreditDTO.getStatus(), requestCreditDTO.getVehicleDTO().getId());
        RequestCredit requestCreditC = requestCreditRepository.findByClientJPQL(requestCreditDTO.getCreatedDate(), requestCreditDTO.getStatus(),requestCreditDTO.getClientDTO().getId());
        if(requestCreditC != null) {
            throw new ResponseException("El cliente ya tiene una solicitud en esta fecha");
        } else if(requestCreditV != null) {
            throw new ResponseException("Ya existe una solicitud de credito para el automovil ");
        } else {

            RequestCredit requestCredit1 = requestCreditRepository.save(convertEntity(requestCreditDTO));
            //List<Executive> executive = executiveRepository.findByNumberPoint(requestCredit1.getCarYard().getNumberPoint());
            requestCreditDTO.setId(requestCredit1.getId());
            return requestCreditDTO;
        }

    }

    private Vehicle convertEntityVehicle(VehicleDTO vehicleDTO){
        return mapper.map(vehicleDTO, Vehicle.class);
    }
    private RequestCredit convertEntity(RequestCreditDTO requestCreditDTO){
        return mapper.map(requestCreditDTO, RequestCredit.class);
    }

    private ExecutiveDTO convertDTOExecutives(){

        return null;
    }
}
