package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.Enums.CreditSubjectType;
import com.bancopichincha.credito.automotriz.domain.dto.ClientDTO;
import com.bancopichincha.credito.automotriz.domain.entities.Client;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.ClientRepository;
import com.bancopichincha.credito.automotriz.service.ClientService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    @PostConstruct
    public void loader() {
        this.loadCSV();
    }

    private final ClientRepository clientRepository;

    private ModelMapper mapper = new ModelMapper();

    public void loadCSV() {
        FileReader fileCSV = null;
        CSVReader csvReader = null;
        try {
            String[] data = null;
            List<Client> clients = new ArrayList<>();
            Resource resource = new ClassPathResource("/initialData/clientes.csv");
            fileCSV = new FileReader(resource.getURL().getPath());
            CSVParser csvParser = new CSVParserBuilder().withSeparator(',').build();
            csvReader = new CSVReaderBuilder(fileCSV).withCSVParser(csvParser).build();

            while ((data = csvReader.readNext()) != null) {
                clients.add(cvsToClient(data));
            }
            clientRepository.saveAll(clients);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ClientDTO save(ClientDTO clientDTO) {
        Client client = clientRepository.findByDni(clientDTO.getDni()).orElse(null);
        if(client == null){
            clientRepository.save(convertEntity(clientDTO));
            return clientDTO;
        } else {
            throw  new ResponseException("Cliente ya registrado");
        }
    }

    @Override
    public ClientDTO get(String id){
        Optional<Client> client = clientRepository.findById(UUID.fromString(id));
        if(!client.isPresent()){
            throw new ResponseException("Usuario no encontrado");
        }
        return convertDTO(client.get());
    }

    @Override
    public List<ClientDTO> getAll() {
        List<ClientDTO> clientDTOS = new ArrayList<>();
        List<Client> clients = clientRepository.findAll();
        clients.forEach(client -> {
            clientDTOS.add(convertDTO(client));
        });
        return clientDTOS;
    }

    @Override
    public ClientDTO update(ClientDTO clientDTO) {
        Optional<Client> clientQuery = clientRepository.findById(clientDTO.getId());
        if(!clientQuery.isPresent()){
            throw new ResponseException("Cliente no existe");
        }
        Client client = convertEntity(clientDTO);
        clientRepository.save(client);
        return convertDTO(client);
    }

    @Override
    public String delete(String id) {
        Optional<Client> client = clientRepository.findById(UUID.fromString(id));
        if(!client.isPresent()){
            throw new ResponseException("Cliente no existe");
        }
        if(!client.get().getAssignmentClientCarYards().isEmpty() || !client.get().getRequestCredits().isEmpty()){
            throw  new ResponseException("Cliente no puede ser eliminado debido  que tiene procesos asociados");
        }
        clientRepository.deleteById(UUID.fromString(id));
        return "Cliente eliminado correctamente";
    }


    private ClientDTO convertDTO(Client client){
            return mapper.map(client, ClientDTO.class);
        }
    private Client convertEntity(ClientDTO clientDTO){
        return mapper.map(clientDTO, Client.class);
    }

    private Client cvsToClient(String[] data) throws ParseException {
        Client client = new Client();
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        client.setDni(data[0]);
        client.setFirstName(data[1]);
        client.setLastName(data[2]);
        client.setAge(Integer.valueOf(data[3]));
        client.setBirthday(format.parse(data[4]));
        client.setAddress(data[5]);
        client.setCelPhone(data[6]);
        client.setCivilStatus(data[7]);
        client.setDniPartner(data[8] != null ? data[8] : null);
        client.setNamePartner(data[9] != null ? data[9] : null);
        if (data[10] == "SI") {
            client.setCreditSubject(CreditSubjectType.CREDIT_SUBJECT);
        } else {
            client.setCreditSubject(CreditSubjectType.NOT_CREDIT_SUBJECT);
        }
        return client;
    }
}
