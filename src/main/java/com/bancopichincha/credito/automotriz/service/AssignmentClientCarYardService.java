package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.AssignmentClientCarYardDTO;

import java.util.List;

public interface AssignmentClientCarYardService {

    AssignmentClientCarYardDTO save(AssignmentClientCarYardDTO assignmentClientCarYardDTO);
    AssignmentClientCarYardDTO get(String id);
    List<AssignmentClientCarYardDTO> getAll();
    AssignmentClientCarYardDTO update(AssignmentClientCarYardDTO assignmentClientCarYardDTO);
    String delete(String id);
}
