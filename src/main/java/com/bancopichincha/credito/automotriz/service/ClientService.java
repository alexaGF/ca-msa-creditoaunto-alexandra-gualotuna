package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.ClientDTO;

import java.util.List;

public interface ClientService {

    ClientDTO save(ClientDTO clientDTO);
    ClientDTO get(String id);
    List<ClientDTO> getAll();
    ClientDTO update(ClientDTO clientDTO);
    String delete(String id);
}
