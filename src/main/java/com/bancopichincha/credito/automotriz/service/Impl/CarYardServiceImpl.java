package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.CarYardDTO;
import com.bancopichincha.credito.automotriz.domain.entities.CarYard;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.CarYardRepository;
import com.bancopichincha.credito.automotriz.service.CarYardService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CarYardServiceImpl implements CarYardService {

    private final CarYardRepository carYardRepository;

    private ModelMapper mapper = new ModelMapper();
    @Override
    public CarYardDTO save(CarYardDTO yardDTO) {
        CarYard carYard = carYardRepository.findByNumberPoint(yardDTO.getNumberPoint()).orElse(null);
        if(carYard == null){
            carYardRepository.save(convertEntity(yardDTO));
            return yardDTO;
        } else {
            throw new ResponseException("Patio ya registrado");
        }
    }

    @Override
    public CarYardDTO get(String id) {
        Optional<CarYard> carYard = carYardRepository.findById(UUID.fromString(id));
        if(!carYard.isPresent()){
            throw new ResponseException("Patio no encontrado");
        }
        return convertDTO(carYard.get());
    }

    @Override
    public List<CarYardDTO> getAll() {
        List<CarYardDTO> yardDTOS = new ArrayList<>();
        List<CarYard> carYards = carYardRepository.findAll();
        carYards.forEach(brand -> {
            yardDTOS.add(convertDTO(brand));
        });
        return yardDTOS;
    }

    @Override
    public CarYardDTO update(CarYardDTO yardDTO) {
        Optional<CarYard> carYard = carYardRepository.findById(yardDTO.getId());
        if(!carYard.isPresent()){
            throw new ResponseException("Patio no encontrado");
        }
        CarYard yard = convertEntity(yardDTO);
        carYardRepository.save(yard);
        return convertDTO(yard);
    }

    @Override
    public String delete(String id) {
        Optional<CarYard> carYard = carYardRepository.findById(UUID.fromString(id));
        if(!carYard.isPresent()){
            throw new ResponseException("Patio no encontrado");
        }
        if(!carYard.get().getAssignmentClientCarYards().isEmpty() || !carYard.get().getRequestCredits().isEmpty()){
            throw  new ResponseException("Patio no puede ser eliminado debido  que tiene procesos asociados");
        }
        carYardRepository.deleteById(UUID.fromString(id));
        return "Patio eliminado correctamente";
    }

    private CarYardDTO convertDTO(CarYard carYard){
        return mapper.map(carYard, CarYardDTO.class);
    }
    private CarYard convertEntity(CarYardDTO yardDTO){
        return mapper.map(yardDTO, CarYard.class);
    }
}
