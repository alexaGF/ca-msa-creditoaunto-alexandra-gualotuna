package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.CarYardDTO;

import java.util.List;

public interface CarYardService {

    CarYardDTO save(CarYardDTO yardDTO);
    CarYardDTO get(String id);
    List<CarYardDTO> getAll();
    CarYardDTO update(CarYardDTO yardDTO);
    String delete(String id);
}
