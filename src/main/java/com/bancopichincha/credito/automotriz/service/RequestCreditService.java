package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.RequestCreditDTO;

public interface RequestCreditService {

    RequestCreditDTO save(RequestCreditDTO requestCreditDTO);
}
