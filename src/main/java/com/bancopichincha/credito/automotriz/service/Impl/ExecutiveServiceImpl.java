package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.domain.entities.Executive;
import com.bancopichincha.credito.automotriz.repository.ExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.FileReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ExecutiveServiceImpl implements ExecutiveService {

    @PostConstruct
    public void loader() {
        this.loadCSV();
    }

    private final ExecutiveRepository executiveRepository;

    private ModelMapper mapper = new ModelMapper();

    public void loadCSV() {
        FileReader fileCSV = null;
        CSVReader csvReader = null;
        try {
            String[] data = null;
            List<Executive> executives = new ArrayList<>();
            Resource resource = new ClassPathResource("/initialData/executives.csv");
            fileCSV = new FileReader(resource.getURL().getPath());
            CSVParser csvParser = new CSVParserBuilder().withSeparator(',').build();
            csvReader = new CSVReaderBuilder(fileCSV).withCSVParser(csvParser).build();

            while ((data = csvReader.readNext()) != null) {
                executives.add(cvsToClient(data));
            }
            executiveRepository.saveAll(executives);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private Executive cvsToClient(String[] data) throws ParseException {
        Executive executive = new Executive();
        executive.setFirstName(data[0]);
        executive.setLastName(data[1]);
        executive.setAddress(data[2]);
        executive.setHomePhone(data[3]);
        executive.setCelPhone(data[4]);
        executive.setNumberPoint(Integer.valueOf(data[5]));
        executive.setAge(Integer.valueOf(data[6]));
        return executive;
    }


    @Override
    public List<ExecutiveDTO> getAll() {
        List<ExecutiveDTO> dtos = new ArrayList<>();
        List<Executive> executives = executiveRepository.findAll();
        executives.forEach(executive -> {
            dtos.add(convertDTO(executive));
        });
        return dtos;
    }

    private ExecutiveDTO convertDTO(Executive executive){
        return mapper.map(executive, ExecutiveDTO.class);
    }
}
