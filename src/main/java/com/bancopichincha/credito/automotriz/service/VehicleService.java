package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.BrandDTO;
import com.bancopichincha.credito.automotriz.domain.dto.VehicleDTO;

import java.util.List;

public interface VehicleService {
    VehicleDTO save(VehicleDTO vehicleDTO);
    List<VehicleDTO> getModel(String id);
    List<VehicleDTO> getBrand(BrandDTO brandDTO);
    List<VehicleDTO> getAll();
    VehicleDTO update(VehicleDTO vehicleDTO);
    String delete(String id);
}
