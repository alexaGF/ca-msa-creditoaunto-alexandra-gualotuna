package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.BrandDTO;

import java.util.List;

public interface BrandService {
    BrandDTO save(BrandDTO brandDTO);
    BrandDTO get(String id);
    List<BrandDTO> getAll();
    BrandDTO update(BrandDTO brandDTO);
    String delete(String id);
}
