package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.BrandDTO;
import com.bancopichincha.credito.automotriz.domain.dto.VehicleDTO;
import com.bancopichincha.credito.automotriz.domain.entities.Brand;
import com.bancopichincha.credito.automotriz.domain.entities.Vehicle;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

    private final VehicleRepository vehicleRepository;

    private ModelMapper mapper = new ModelMapper();
    @Override
    public VehicleDTO save(VehicleDTO vehicleDTO) {
        Vehicle vehicle = vehicleRepository.findByLicensePlate(vehicleDTO.getLicensePlate()).orElse(null);
        if(vehicle == null){
            vehicleRepository.save(convertEntity(vehicleDTO));
            return vehicleDTO;
        } else {
            throw new ResponseException("Vehiculo ya registrado");
        }
    }

    @Override
    public List<VehicleDTO> getModel(String id) {
        List<VehicleDTO> vehicleDTOS = new ArrayList<>();
        List<Vehicle> vehicles = vehicleRepository.findByModel((id));
        vehicles.forEach(vehicle -> {
            vehicleDTOS.add(convertDTO(vehicle));
        });
        return vehicleDTOS;
    }

    @Override
    public List<VehicleDTO> getBrand(BrandDTO brandDTO) {
        List<VehicleDTO> vehicleDTOS = new ArrayList<>();
        List<Vehicle> vehicles = vehicleRepository.findByBrand(convertEntityBrand(brandDTO));
        vehicles.forEach(vehicle1 -> {
            vehicleDTOS.add(convertDTO(vehicle1));
        });
        return vehicleDTOS;
    }

    @Override
    public List<VehicleDTO> getAll() {
        List<VehicleDTO> vehicleDTOS = new ArrayList<>();
        List<Vehicle> vehicles = vehicleRepository.findAll();
        vehicles.forEach(vehicle -> {
            vehicleDTOS.add(convertDTO(vehicle));
        });
        return vehicleDTOS;
    }

    @Override
    public VehicleDTO update(VehicleDTO vehicleDTO) {
        Optional<Vehicle> vehicleQuery = vehicleRepository.findById(vehicleDTO.getId());
        if(!vehicleQuery.isPresent()){
            throw new ResponseException("Vehiculo no existe");
        }
        Vehicle vehicle = convertEntity(vehicleDTO);
        vehicleRepository.save(vehicle);
        return convertDTO(vehicle);
    }

    @Override
    public String delete(String id) {
        Optional<Vehicle> vehicle = vehicleRepository.findById(UUID.fromString(id));
        if(!vehicle.isPresent()){
            throw new ResponseException("Vehiculo no existe");
        }
        if(!vehicle.get().getRequestCredits().isEmpty()){
            throw  new ResponseException("Patio no puede ser eliminado debido  que tiene procesos asociados");
        }
        vehicleRepository.deleteById(UUID.fromString(id));
        return "Vehiculo eliminado correctamente";
    }

    private Brand convertEntityBrand(BrandDTO BrandDTO){
        return mapper.map(BrandDTO, Brand.class);
    }
    private BrandDTO convertDTO(Brand brand){
        return mapper.map(brand, BrandDTO.class);
    }
    private VehicleDTO convertDTO(Vehicle vehicle){
        BrandDTO brandDTO = convertDTO(vehicle.getBrand());
        VehicleDTO dto = new VehicleDTO();
        mapper.map(vehicle, dto);
        dto.setBrandDTO(brandDTO);
        return dto;
    }
    private Vehicle convertEntity(VehicleDTO vehicleDTO){
        return mapper.map(vehicleDTO, Vehicle.class);
    }
}
