package com.bancopichincha.credito.automotriz.service.Impl;

import com.bancopichincha.credito.automotriz.domain.dto.AssignmentClientCarYardDTO;
import com.bancopichincha.credito.automotriz.domain.dto.CarYardDTO;
import com.bancopichincha.credito.automotriz.domain.dto.ClientDTO;
import com.bancopichincha.credito.automotriz.domain.entities.AssignmentClientCarYard;
import com.bancopichincha.credito.automotriz.domain.entities.CarYard;
import com.bancopichincha.credito.automotriz.domain.entities.Client;
import com.bancopichincha.credito.automotriz.exception.ResponseException;
import com.bancopichincha.credito.automotriz.repository.AssignmentClientCarYardRepository;
import com.bancopichincha.credito.automotriz.service.AssignmentClientCarYardService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AssignmentClientCarYardServiceImpl implements AssignmentClientCarYardService {
    private final AssignmentClientCarYardRepository assignmentClientCarYardRepository;
    private ModelMapper mapper = new ModelMapper();

    @Transactional
    @Override
    public AssignmentClientCarYardDTO save(AssignmentClientCarYardDTO assignmentClientCarYardDTO) {
        Client client = convertEntityClient(assignmentClientCarYardDTO.getClientDTO());
        CarYard carYard = convertEntityCarYard(assignmentClientCarYardDTO.getCarYardDTO());
        Optional<AssignmentClientCarYard> assignmentClientCarYard = assignmentClientCarYardRepository.findByClientAndCarYard(client, carYard);
        if(!assignmentClientCarYard.isEmpty()){
            throw new ResponseException("Cliente ya esta asignado a ese patio");
        }
        AssignmentClientCarYard assignmentClientCarYard1 = convertEntity(assignmentClientCarYardDTO);
        return convertDTO(assignmentClientCarYardRepository.save(assignmentClientCarYard1));
    }

    @Override
    public AssignmentClientCarYardDTO get(String id) {
        Optional<AssignmentClientCarYard> assignmentClientCarYard = assignmentClientCarYardRepository.findById(UUID.fromString(id));
        if(!assignmentClientCarYard.isPresent()){
            throw new ResponseException("Assignacion no existe");
        }
        return convertDTO(assignmentClientCarYard.get());
    }

    @Override
    public List<AssignmentClientCarYardDTO> getAll() {
        List<AssignmentClientCarYardDTO> dtos = new ArrayList<>();
        List<AssignmentClientCarYard> assignmentClientCarYards = assignmentClientCarYardRepository.findAll();
        assignmentClientCarYards.forEach(assignmentClientCarYard -> {
            dtos.add(convertDTO(assignmentClientCarYard));
        });
        return dtos;
    }

    @Override
    public AssignmentClientCarYardDTO update(AssignmentClientCarYardDTO assignmentClientCarYardDTO) {
        Optional<AssignmentClientCarYard> assignment = assignmentClientCarYardRepository.findById(assignmentClientCarYardDTO.getId());
        if(!assignment.isPresent()){
            throw new ResponseException("Assignacion no existe");
        }
        AssignmentClientCarYard assignmentClientCarYard = convertEntity(assignmentClientCarYardDTO);
        assignmentClientCarYardRepository.save(assignmentClientCarYard);
        return convertDTO(assignmentClientCarYard);
    }

    @Override
    public String delete(String id) {
        Optional<AssignmentClientCarYard> assignmentClientCarYard = assignmentClientCarYardRepository.findById(UUID.fromString(id));
        if(!assignmentClientCarYard.isPresent()){
            throw new ResponseException("Assignacion no existe");
        }
        assignmentClientCarYardRepository.deleteById(UUID.fromString(id));
        return "Asignacion eliminada correctamente";
    }

    private AssignmentClientCarYardDTO convertDTO(AssignmentClientCarYard assignmentClientCarYard){
        ClientDTO clientDTO = new ClientDTO();
        CarYardDTO carYardDTO = new CarYardDTO();
        AssignmentClientCarYardDTO dto = new AssignmentClientCarYardDTO();
        mapper.map(assignmentClientCarYard.getClient(), clientDTO);
        mapper.map(assignmentClientCarYard.getCarYard(), carYardDTO);
        mapper.map(assignmentClientCarYard, dto);
        dto.setClientDTO(clientDTO);
        dto.setCarYardDTO(carYardDTO);
        return dto;
    }
    private AssignmentClientCarYard convertEntity(AssignmentClientCarYardDTO assignmentClientCarYardDTO){

        return mapper.map(assignmentClientCarYardDTO, AssignmentClientCarYard.class);
    }

    private Client convertEntityClient(ClientDTO clientDTO){
        return mapper.map(clientDTO, Client.class);
    }
    private CarYard convertEntityCarYard(CarYardDTO yardDTO){
        return mapper.map(yardDTO, CarYard.class);
    }

}
