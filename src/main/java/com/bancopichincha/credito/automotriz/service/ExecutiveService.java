package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.dto.ExecutiveDTO;

import java.util.List;

public interface ExecutiveService {
    List<ExecutiveDTO> getAll();

}
