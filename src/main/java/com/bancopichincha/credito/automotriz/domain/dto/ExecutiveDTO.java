package com.bancopichincha.credito.automotriz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ExecutiveDTO {
    private UUID id;
    private String dni;
    private String firstName;
    private String lastName;
    private String address;
    private String homePhone;
    private String celPhone;
    private Integer numberPoint;
    private CarYardDTO carYardDTO;
    private Integer age;
}
