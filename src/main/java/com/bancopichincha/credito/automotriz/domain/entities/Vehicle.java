package com.bancopichincha.credito.automotriz.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vehicles")
public class Vehicle {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String licensePlate;
    private String model;
    private String chassisNumber;
    @ManyToOne
    @JoinColumn(name = "brand_id")
    private Brand brand;
    private String type;
    private long cylinderCapacity;
    private String value;
    @ManyToOne
    @JoinColumn(name = "car_yard_id")
    private CarYard carYard;
    @OneToMany(mappedBy = "vehicle")
    private List<RequestCredit> requestCredits;

}
