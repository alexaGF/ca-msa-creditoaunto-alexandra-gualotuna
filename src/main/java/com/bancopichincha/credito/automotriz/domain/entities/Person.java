package com.bancopichincha.credito.automotriz.domain.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor
public class Person {
    private String dni;
    private String firstName;
    private String lastName;
    private String address;
    private Integer age;
    private String celPhone;

}
