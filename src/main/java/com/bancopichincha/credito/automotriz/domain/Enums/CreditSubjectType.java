package com.bancopichincha.credito.automotriz.domain.Enums;

public enum CreditSubjectType {
    CREDIT_SUBJECT, NOT_CREDIT_SUBJECT
}
