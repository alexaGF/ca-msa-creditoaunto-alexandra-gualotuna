package com.bancopichincha.credito.automotriz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AssignmentClientCarYardDTO {
    private UUID id;
    @NotNull(message = "Campo obligatorio")
    private ClientDTO clientDTO;
    @NotNull(message = "Campo obligatorio")
    private CarYardDTO carYardDTO;
    @NotNull(message = "Campo obligatorio")
    private Date assignmentDate;
}
