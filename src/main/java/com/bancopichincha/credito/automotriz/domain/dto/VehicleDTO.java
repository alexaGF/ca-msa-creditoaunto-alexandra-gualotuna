package com.bancopichincha.credito.automotriz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class VehicleDTO {
    private UUID id;
    @NotNull(message = "Campo obligatorio")
    private String licensePlate;
    @NotNull(message = "Campo obligatorio")
    private String model;
    @NotNull(message = "Campo obligatorio")
    private String chassisNumber;
    @NotNull(message = "Campo obligatorio")
    private BrandDTO brandDTO;
    private String type;
    @NotNull(message = "Campo obligatorio")
    private long cylinderCapacity;
    @NotNull(message = "Campo obligatorio")
    private String value;
    private CarYardDTO carYardDTO;
    private List<RequestCreditDTO> requestCreditDTOS;
}
