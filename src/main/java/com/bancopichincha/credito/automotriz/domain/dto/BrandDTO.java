package com.bancopichincha.credito.automotriz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.UUID;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BrandDTO {
    private UUID id;
    @NotNull(message = "Campo obligatorio")
    private String name;
}
