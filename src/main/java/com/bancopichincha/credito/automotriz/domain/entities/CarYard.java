package com.bancopichincha.credito.automotriz.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "car_yard")
public class CarYard {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String name;
    private String address;
    private String phone;
    private Integer numberPoint;

    @OneToMany(mappedBy = "carYard")
    private List<Vehicle> vehicles;
    @OneToMany(mappedBy = "carYard")
    private List<RequestCredit> requestCredits;
    @OneToMany(mappedBy = "carYard")
    //redundancia
    private List<AssignmentClientCarYard> assignmentClientCarYards;

}