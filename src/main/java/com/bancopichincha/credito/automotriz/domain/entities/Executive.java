package com.bancopichincha.credito.automotriz.domain.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "executives")
public class Executive extends Person {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String homePhone;
    private Integer numberPoint;
    @ManyToOne
    @JoinColumn(name = "car_yard_id")
    private CarYard carYard;

}
