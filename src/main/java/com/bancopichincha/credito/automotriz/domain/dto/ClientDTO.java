package com.bancopichincha.credito.automotriz.domain.dto;


import com.bancopichincha.credito.automotriz.domain.Enums.CreditSubjectType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ClientDTO {

    private UUID id;
    @NotNull(message = "Campo obligatorio")
    private String dni;
    @NotNull(message = "Campo obligatorio")
    private String firstName;
    @Max(value = 50, message = "Maximo 50 años")
    @Min(value = 18, message = "Edad minimo 18 años")
    @NotNull(message = "Campo obligatorio")
    private Integer age;
    @NotNull(message = "Campo obligatorio")
    private Date birthday;
    @NotNull(message = "Campo obligatorio")
    private String lastName;
    @NotNull(message = "Campo obligatorio")
    private String address;
    @NotNull(message = "Campo obligatorio")
    private String phone;
    @NotNull(message = "Campo obligatorio")
    private String civilStatus;
    @NotNull(message = "Campo obligatorio")
    private String dniPartner;
    @NotNull(message = "Campo obligatorio")
    private String namePartner;
    @NotNull(message = "Campo obligatorio")
    private CreditSubjectType creditSubject;

}
