package com.bancopichincha.credito.automotriz.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RequestCreditDTO {
    private UUID id;
    @NotNull(message = "Campo obligatorio")
    private Date createdDate;
    @NotNull(message = "Campo obligatorio")
    private ClientDTO clientDTO;
    @NotNull(message = "Campo obligatorio")
    private CarYardDTO carYardDTO;
    @NotNull(message = "Campo obligatorio")
    private VehicleDTO vehicleDTO;
    @NotNull(message = "Campo obligatorio")
    private Integer entryFree;
    @NotNull(message = "Campo obligatorio")
    private Integer quote;
    @NotNull(message = "Campo obligatorio")
    private BigDecimal monthsTerm;
    @NotNull(message = "Campo obligatorio")
    private ExecutiveDTO executiveDTO;
    private String observation;
    @NotNull(message = "Campo obligatorio")
    private String status;
}
