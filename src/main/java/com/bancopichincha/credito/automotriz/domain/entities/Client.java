package com.bancopichincha.credito.automotriz.domain.entities;

import com.bancopichincha.credito.automotriz.domain.Enums.CreditSubjectType;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "clients")
public class Client extends Person {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private Date birthday;
    private String civilStatus;
    private String dniPartner;
    private String namePartner;
    private CreditSubjectType creditSubject;

    @OneToMany(mappedBy = "client")
    private List<RequestCredit> requestCredits;

    @OneToMany(mappedBy = "client")
    private List<AssignmentClientCarYard> assignmentClientCarYards;

}
