package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.BrandDTO;
import com.bancopichincha.credito.automotriz.domain.dto.VehicleDTO;
import com.bancopichincha.credito.automotriz.service.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/vehicles")
public class VehicleController {

    private final VehicleService vehicleService;

    @PostMapping
    public ResponseEntity<VehicleDTO> save(@RequestBody @Valid VehicleDTO vehicleDTO){
        return ResponseEntity.ok(vehicleService.save(vehicleDTO));
    }

    @GetMapping("/getModel")
    public ResponseEntity<List<VehicleDTO>> getModel(@RequestParam String model){
        return ResponseEntity.ok(vehicleService.getModel(model));

    }

    @GetMapping("/getBrand")
    public ResponseEntity<List<VehicleDTO>> getBrand(@RequestBody BrandDTO brandDTO){
        return ResponseEntity.ok(vehicleService.getBrand(brandDTO));
    }
    @GetMapping
    public ResponseEntity<List<VehicleDTO>> getAll(){
        return ResponseEntity.ok(vehicleService.getAll());

    }

    @PutMapping
    public ResponseEntity<VehicleDTO> update(@RequestBody @Valid VehicleDTO vehicleDTO){
        return ResponseEntity.ok(vehicleService.update(vehicleDTO));

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id){
        return ResponseEntity.ok(vehicleService.delete(id));

    }
}
