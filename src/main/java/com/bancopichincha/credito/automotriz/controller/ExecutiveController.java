package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.ExecutiveDTO;
import com.bancopichincha.credito.automotriz.service.ExecutiveService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("executives")
public class ExecutiveController {
    private final ExecutiveService executiveService;

    @GetMapping
    public ResponseEntity<List<ExecutiveDTO>> get(){
        return ResponseEntity.ok(executiveService.getAll());

    }
}
