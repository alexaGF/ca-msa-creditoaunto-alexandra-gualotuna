package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.ClientDTO;
import com.bancopichincha.credito.automotriz.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/clients")
public class ClientController {
    private final ClientService clientService;

    @PostMapping
    public ResponseEntity<ClientDTO> save(@RequestBody @Valid ClientDTO clientDTO){
        return ResponseEntity.ok(clientService.save(clientDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientDTO> get(@PathVariable String id) {
        return ResponseEntity.ok(clientService.get(id));
    }
    @GetMapping
    public ResponseEntity<List<ClientDTO>> getAll(){

        return ResponseEntity.ok(clientService.getAll());
    }

    @PutMapping
    public ResponseEntity<ClientDTO> update(@RequestBody @Valid ClientDTO clientDTO){
        return ResponseEntity.ok(clientService.update(clientDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id){
        return ResponseEntity.ok(clientService.delete(id));
    }


}

