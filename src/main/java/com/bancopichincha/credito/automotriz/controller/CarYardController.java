package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.CarYardDTO;
import com.bancopichincha.credito.automotriz.service.CarYardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/carYards")
public class CarYardController {
    private final CarYardService carYardService;

    @PostMapping
    public ResponseEntity<CarYardDTO> save(@RequestBody @Valid CarYardDTO yardDTO){
        return ResponseEntity.ok(carYardService.save(yardDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarYardDTO> get(@PathVariable String id){
        return ResponseEntity.ok(carYardService.get(id));
    }
    @GetMapping
    public ResponseEntity<List<CarYardDTO>> getAll(){
        return ResponseEntity.ok(carYardService.getAll());
    }

    @PutMapping
    public ResponseEntity<CarYardDTO> update(@RequestBody @Valid CarYardDTO yardDTO){
        return ResponseEntity.ok(carYardService.update(yardDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id){
        return ResponseEntity.ok(carYardService.delete(id));
    }

}
