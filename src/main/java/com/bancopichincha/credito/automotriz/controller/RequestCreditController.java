package com.bancopichincha.credito.automotriz.controller;


import com.bancopichincha.credito.automotriz.domain.dto.RequestCreditDTO;
import com.bancopichincha.credito.automotriz.service.RequestCreditService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/requestCredits")
public class RequestCreditController {
    private final RequestCreditService requestCreditService;
    @PostMapping()
    public ResponseEntity<RequestCreditDTO> save(@RequestBody @Valid RequestCreditDTO requestCreditDTO){
        return ResponseEntity.ok(requestCreditService.save(requestCreditDTO));
    }

}
