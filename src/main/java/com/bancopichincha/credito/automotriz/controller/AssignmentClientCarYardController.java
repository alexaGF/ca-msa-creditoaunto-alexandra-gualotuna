package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.AssignmentClientCarYardDTO;
import com.bancopichincha.credito.automotriz.service.AssignmentClientCarYardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/assignments")
public class AssignmentClientCarYardController {
    private final AssignmentClientCarYardService assignmentClientCarYardService;

    @PostMapping
    public ResponseEntity<AssignmentClientCarYardDTO> save(@RequestBody @Valid AssignmentClientCarYardDTO assignmentClientCarYardDTO){
        return ResponseEntity.ok(assignmentClientCarYardService.save(assignmentClientCarYardDTO));
    }
    @GetMapping
    public ResponseEntity<List<AssignmentClientCarYardDTO>> getAll(){
        return ResponseEntity.ok(assignmentClientCarYardService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<AssignmentClientCarYardDTO> get(@PathVariable String id){
        return ResponseEntity.ok(assignmentClientCarYardService.get(id));
    }

    @PutMapping
    public ResponseEntity<AssignmentClientCarYardDTO> update(@RequestBody @Valid AssignmentClientCarYardDTO assignmentClientCarYardDTO){
        return ResponseEntity.ok(assignmentClientCarYardService.update(assignmentClientCarYardDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id){
        return ResponseEntity.ok(assignmentClientCarYardService.delete(id));
    }
}
