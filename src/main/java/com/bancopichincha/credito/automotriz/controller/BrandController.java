package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.dto.BrandDTO;
import com.bancopichincha.credito.automotriz.service.BrandService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/brands")
public class BrandController {
    private final BrandService brandService;

    @PostMapping()
    public ResponseEntity<BrandDTO> save(@RequestBody @Valid BrandDTO brandDTO){
        return ResponseEntity.ok(brandService.save(brandDTO));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BrandDTO> get(@PathVariable String id){
        return ResponseEntity.ok(brandService.get(id));
    }
    @GetMapping
    public ResponseEntity<List<BrandDTO>> getAll(){
        return ResponseEntity.ok(brandService.getAll());
    }

    @PutMapping
    public ResponseEntity<BrandDTO> update(@RequestBody @Valid BrandDTO brandDTO){
        return ResponseEntity.ok(brandService.update(brandDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable String id){
        return ResponseEntity.ok(brandService.delete(id));
    }
}
