package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.entities.CarYard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarYardRepository extends JpaRepository<CarYard, UUID> {
    Optional<CarYard> findByNumberPoint(Integer number);
}
