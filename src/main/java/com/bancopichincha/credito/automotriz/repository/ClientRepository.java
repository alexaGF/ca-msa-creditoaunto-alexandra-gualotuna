package com.bancopichincha.credito.automotriz.repository;


import com.bancopichincha.credito.automotriz.domain.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ClientRepository extends JpaRepository<Client, UUID> {
    Optional<Client> findByDni(String dni);
}
