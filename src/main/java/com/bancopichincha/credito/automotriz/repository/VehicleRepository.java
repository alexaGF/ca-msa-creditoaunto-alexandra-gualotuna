package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.entities.Brand;
import com.bancopichincha.credito.automotriz.domain.entities.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, UUID> {
    Optional<Vehicle> findByLicensePlate(String licensePlate);
    List<Vehicle> findByModel(String model);
    List<Vehicle> findByBrand(Brand brand);

}
