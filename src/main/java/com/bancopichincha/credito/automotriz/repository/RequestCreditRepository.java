package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.entities.RequestCredit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.UUID;

@Repository
public interface RequestCreditRepository extends JpaRepository<RequestCredit, UUID> {
    @Query("select r from RequestCredit r INNER JOIN r.vehicle v where r.status = :status AND v.id = :vid")
    RequestCredit findByVehicleJPQL(@Param("status") String status, @Param("vid") UUID vid);

    @Query("select r from RequestCredit r INNER JOIN r.client c where r.createdDate = :date AND r.status = :status AND c.id = :cid")
    RequestCredit findByClientJPQL(@Param("date") Date date, @Param("status") String status,@Param("cid") UUID cid);
}
