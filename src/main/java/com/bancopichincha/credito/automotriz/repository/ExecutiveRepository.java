package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.entities.Executive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ExecutiveRepository extends JpaRepository<Executive, UUID> {
    @Query("select e from Executive e where e.numberPoint = :number")
    List<Executive> findByNumberPoint(@Param("number") Integer number);

}
